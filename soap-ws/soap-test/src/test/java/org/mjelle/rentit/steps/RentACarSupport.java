package org.mjelle.rentit.steps;

import java.net.MalformedURLException;
import java.net.URL;
import org.mjelle.rentit.soap.RentCar;
import org.mjelle.rentit.soap.RentCarService;

public class RentACarSupport {

    final String port = System.getProperty("server.http.port");

    private final RentCar rentCar;

    public RentACarSupport() {
        String protocol = "http://";
        String host = "localhost";
        String context = "/soap-main";
        String serviceEndpoint = "/webservices/SoapRentCar?WSDL";
        String endpointAddress = protocol + host + ":" + port + context + serviceEndpoint;

        RentCarService service;
        try {
            service = new RentCarService(new URL(endpointAddress));
        } catch (MalformedURLException ex) {
            throw new IllegalStateException("MalformedURLException caught", ex);
        }
        rentCar = service.getRentCarPort();
    }

    public void createCars(int availableCars) {
        rentCar.createCars(availableCars);
    }

    public void rentACar() {
        rentCar.rentCar();
    }

    public int getAvailableNumberOfCars() {
        return rentCar.getAvailableCars();
    }
}

package org.mjelle.rentit.soap;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;
import org.mjelle.rentit.model.entity.Car;
import org.mjelle.rentit.service.CarService;

@Stateless
@WebService(serviceName = "RentCarService", portName = "RentCarPort")
public class SoapRentCar implements RentCar {

    @EJB
    private CarService carService;

    @Override
    public void createCars(Integer numberOfCars) {
        for (int i = 0; i < numberOfCars; i++) {
            Car car = new Car();
            carService.create(car);
        }
    }

    @Override
    public void rentCar() {
        Car car = carService.findAvailableCar();
        car.setRented(true);
        carService.update(car);
    }

    @Override
    public Integer getAvailableCars() {
        return carService.getNumberOfAvailableCars();
    }
}

package org.mjelle.rentit;

import java.io.File;
import javax.xml.ws.WebServiceRef;
import static org.hamcrest.core.Is.is;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mjelle.rentit.soap.RentCar;

@RunWith(Arquillian.class)
public class RentCarTest {

    @WebServiceRef
    private RentCar rentCar;
    
    @Deployment
    public static WebArchive createDeployment() {
        File[] files = Maven.resolver().loadPomFromFile("pom.xml")
                .importRuntimeDependencies().resolve().withTransitivity().asFile();

        WebArchive war = ShrinkWrap.create(WebArchive.class, "soap-main.war");
        war.merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class)
                .importDirectory("src/main/webapp").as(GenericArchive.class),
                "/", Filters.includeAll());

        war.addPackages(false, "org.mjelle.rentit.soap");
        war.addAsLibraries(files);
        return war;
    }
    
    @Test
    public void shouldRentACar() {
        int initialNumberOfCars = 43;

        rentCar.createCars(initialNumberOfCars);

        rentCar.rentCar();

        int oneRentedCar = 1;
        Integer expected = initialNumberOfCars - oneRentedCar;

        Integer actual = rentCar.getAvailableCars();

        assertThat(actual, is(expected));
    }
}

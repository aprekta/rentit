package org.mjelle.rentit.steps;

import com.jayway.restassured.RestAssured;
import static com.jayway.restassured.RestAssured.expect;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;

public class RentACarSupport {

    final String port = System.getProperty("server.http.port");

    public RentACarSupport() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = Integer.parseInt(port);
        RestAssured.basePath = "/rest-main/rentit/car";
    }

    public void createCars(int availableCars) {
        String path = "/create/" + availableCars;
        expect().statusCode(201).when().post(path);
    }

    public void rentACar() {
        String path = "/rent";
        expect().statusCode(200).when().post(path);
    }

    public int getAvailableNumberOfCars() {
        String path = "/availableCars";
        Response response = expect().statusCode(200).when().get(path);
        ResponseBody body = response.getBody();
        String availableCarsAsString = body.asString();

        return Integer.parseInt(availableCarsAsString);
    }
}

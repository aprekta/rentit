package org.mjelle.rentit;

import java.io.File;
import javax.inject.Inject;
import org.junit.Test;
import org.mjelle.rentit.rest.RentCar;
import static org.hamcrest.core.Is.is;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertThat;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class RentCarTest {
    
    @Inject
    private RentCar rentCar;

    @Deployment
    public static WebArchive createDeployment() {
        File[] files = Maven.resolver().loadPomFromFile("pom.xml")
                .importRuntimeDependencies().resolve().withTransitivity().asFile();

        WebArchive war = ShrinkWrap.create(WebArchive.class, "rest-main.war");
        war.merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class)
                .importDirectory("src/main/webapp").as(GenericArchive.class),
                "/", Filters.includeAll());

        war.addPackages(false, "org.mjelle.rentit", "org.mjelle.rentit.rest");
        war.addAsLibraries(files);
        return war;
    }

    @Test
    public void shouldRentACar() {
        int initialNumberOfCars = 43;

        rentCar.createCars(initialNumberOfCars);

        rentCar.rentCar();

        int oneRentedCar = 1;
        Integer expected = initialNumberOfCars - oneRentedCar;

        String actualString = (String) rentCar.getAvailableCars().getEntity();

        Integer actual = Integer.parseInt(actualString);

        assertThat(actual, is(expected));
    }
}

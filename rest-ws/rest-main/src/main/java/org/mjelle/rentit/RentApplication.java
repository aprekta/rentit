package org.mjelle.rentit;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rentit")
public class RentApplication extends Application {
}

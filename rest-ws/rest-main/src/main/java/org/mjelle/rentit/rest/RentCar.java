package org.mjelle.rentit.rest;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import org.mjelle.rentit.model.entity.Car;
import org.mjelle.rentit.service.CarService;

@Path("/car")
public class RentCar {
    
    @EJB
    private CarService carService;

    @POST
    @Path("/create/{param}")
    public Response createCars(@PathParam("param") Integer numberOfCars) {
        for (int i = 0; i < numberOfCars; i++) {
            Car car = new Car();
            carService.create(car);
        }

        return Response.status(201).build();
    }

    @POST
    @Path("/rent")
    public Response rentCar() {
        Car car = carService.findAvailableCar();
        car.setRented(true);
        carService.update(car);

        return Response.status(200).build();
    }

    @GET
    @Path("/availableCars")
    public Response getAvailableCars() {
        int availableCars = carService.getNumberOfAvailableCars();

        return Response.status(200).entity("" + availableCars).build();
    }
}
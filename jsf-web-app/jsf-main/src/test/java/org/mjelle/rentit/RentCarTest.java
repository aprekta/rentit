package org.mjelle.rentit;

import java.io.File;
import javax.inject.Inject;
import static org.hamcrest.CoreMatchers.is;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mjelle.rentit.view.Available;
import org.mjelle.rentit.view.Create;
import org.mjelle.rentit.view.Rent;

@RunWith(Arquillian.class)
public class RentCarTest {

    @Inject
    private Create create;

    @Inject
    private Rent rent;

    @Inject
    private Available available;

    @Deployment
    public static WebArchive createDeployment() {
        File[] files = Maven.resolver().loadPomFromFile("pom.xml")
                .importRuntimeDependencies().resolve().withTransitivity().asFile();

        WebArchive war = ShrinkWrap.create(WebArchive.class, "jsf-main.war");
        war.merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class)
                .importDirectory("src/main/webapp").as(GenericArchive.class),
                "/", Filters.includeAll());

        war.addPackages(false, "org.mjelle.rentit", "org.mjelle.rentit.controller", "org.mjelle.rentit.view");
        war.addAsLibraries(files);
        return war;
    }

    @Test
    public void shouldRentACar() {
        int initialNumberOfCars = 43;

        create.setNumberOfCars(initialNumberOfCars);
        create.create();

        int oneRentedCar = 1;
        int expected = initialNumberOfCars - oneRentedCar;

        rent.rent();

        int actual = available.getAvailableCars();

        assertThat(actual, is(expected));
    }
}

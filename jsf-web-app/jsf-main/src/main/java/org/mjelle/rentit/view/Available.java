package org.mjelle.rentit.view;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.inject.Inject;
import org.mjelle.rentit.controller.Controller;

@RequestScoped
@Named("available")
public class Available {

    @Inject
    private Controller controller;
    
    public int getAvailableCars() {
        return controller.getNumberOfAvailableCars();
    }
}

package org.mjelle.rentit.view;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.mjelle.rentit.controller.Controller;

@RequestScoped
@Named("create")
public class Create {

    @Inject
    private Controller controller;
    
    private int numberOfCars;
    
    public String create() {
        for (int i = 0; i < numberOfCars; i++) {
            controller.createCar();
        }
        return "available.xhtml";
    }

    public void setNumberOfCars(int numberOfCars) {
        this.numberOfCars = numberOfCars;
    }

    public int getNumberOfCars() {
        return numberOfCars;
    }
}

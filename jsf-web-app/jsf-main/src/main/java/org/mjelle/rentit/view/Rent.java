package org.mjelle.rentit.view;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.mjelle.rentit.controller.Controller;

@RequestScoped
@Named("rent")
public class Rent {

    @Inject
    private Controller controller;
    
    public String rent() {
        controller.rentCar();

        return "available.xhtml";
    }
}

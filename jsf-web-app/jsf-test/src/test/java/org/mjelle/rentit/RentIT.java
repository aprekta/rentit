package org.mjelle.rentit;

import cucumber.api.CucumberOptions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.arquillian.ArquillianCucumber;
import java.io.File;
import java.net.URL;
import static org.hamcrest.CoreMatchers.is;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mjelle.rentit.steps.RentACarSupport;
import org.openqa.selenium.WebDriver;

@RunWith(ArquillianCucumber.class)
@CucumberOptions(format = {"pretty", "html:target/cucumber"})
public class RentIT {
    
    private final RentACarSupport rentACarSupport = new RentACarSupport();

    @Drone
    private WebDriver webDriver;
    
    @ArquillianResource
    private URL url;
    
    @Deployment(testable = false)
    public static WebArchive createDeployment() {
        File archive = Maven.resolver().loadPomFromFile("pom.xml").resolve("org.mjelle.rentit:jsf-main:war:?").withoutTransitivity().asSingleFile();
        WebArchive war = ShrinkWrap.create(ZipImporter.class, "jsf-main.war").importFrom(archive)
                .as(WebArchive.class);
        return war;
    }
    
    @Before
    public void setup() {
        rentACarSupport.setWebDriver(webDriver);
        rentACarSupport.setDeploymentUrl(url.toExternalForm());
    }
    
    @Given("^there are (\\d+) cars available for rental$")
    public void there_are_cars_available_for_rental(int availableCars) throws Throwable {
        rentACarSupport.createCars(availableCars);
    }

    @When("^I rent one$")
    public void rent_one_car() throws Throwable {
        rentACarSupport.rentACar();
    }

    @Then("^there will only be (\\d+) cars available for rental$")
    public void there_will_be_less_cars_available_for_rental(int expectedAvailableCars) throws Throwable {
        int actualAvailableCars = rentACarSupport.getAvailableNumberOfCars();
        assertThat(actualAvailableCars, is(expectedAvailableCars));
    }
}

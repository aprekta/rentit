package org.mjelle.rentit.steps;

import static org.jboss.arquillian.graphene.Graphene.guardHttp;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RentACarSupport {

    private WebDriver webDriver;
    private String deploymentUrl;

    public void createCars(int availableCars) {
        webDriver.get(deploymentUrl + "create.xhtml");

        WebElement numberOfCarsToCreate = webDriver.findElement(By.id("create:numberOfCars"));
        numberOfCarsToCreate.clear();
        numberOfCarsToCreate.sendKeys("" + availableCars);

        WebElement createButton = webDriver.findElement(By.id("create:createButton"));
        guardHttp(createButton).click();
    }

    public void rentACar() {
        webDriver.get(deploymentUrl + "rent.xhtml");

        WebElement rentButton = webDriver.findElement(By.id("rent:rentButton"));
        guardHttp(rentButton).click();
    }

    public int getAvailableNumberOfCars() {
        webDriver.get(deploymentUrl + "available.xhtml");

        WebElement availableCars = webDriver.findElement(By.id("availableCars"));
        String availableCarsString = availableCars.getText();

        return Integer.parseInt(availableCarsString);
    }

    public void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void setDeploymentUrl(String deploymentUrl) {
        this.deploymentUrl = deploymentUrl;
    }
}

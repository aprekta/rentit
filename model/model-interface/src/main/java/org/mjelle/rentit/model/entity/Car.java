package org.mjelle.rentit.model.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
    @NamedQuery(name = Car.NUMBER_OF_AVAILABLE_CARS, query = "SELECT COUNT(c) FROM Car c WHERE c.rented = false"),
})
public class Car implements Serializable {

    public static final String NUMBER_OF_AVAILABLE_CARS = "Car.getNumberOfAvailableCars";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean rented;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setRented(boolean rented) {
        this.rented = rented;
    }

    public boolean isRented() {
        return rented;
    }
}

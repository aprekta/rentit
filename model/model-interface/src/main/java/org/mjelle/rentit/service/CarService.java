package org.mjelle.rentit.service;

import org.mjelle.rentit.model.entity.Car;

public interface CarService {

    public void create(Car car);

    Car findAvailableCar();

    int getNumberOfAvailableCars();
    
    void update(Car car);
}

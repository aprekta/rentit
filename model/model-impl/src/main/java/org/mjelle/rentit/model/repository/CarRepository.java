package org.mjelle.rentit.model.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.mjelle.rentit.model.entity.Car;

@Repository(forEntity = Car.class)
public interface CarRepository extends EntityRepository<Car, Long> {

    Car findAnyByRentedEqual(boolean rented);
    
    @Query(named = Car.NUMBER_OF_AVAILABLE_CARS)
    Long getNumberOfAvailableCars();
}

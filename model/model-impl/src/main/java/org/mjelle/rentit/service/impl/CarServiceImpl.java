package org.mjelle.rentit.service.impl;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.mjelle.rentit.model.entity.Car;
import org.mjelle.rentit.model.repository.CarRepository;
import org.mjelle.rentit.service.CarService;

@Singleton
@LocalBean
@Remote(CarService.class)
@Transactional
public class CarServiceImpl implements CarService {

    @Inject
    private EntityManager em;
    
    @Inject
    private CarRepository carRepository;
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void create(Car car) {
        carRepository.save(car);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Car findAvailableCar() {
        return carRepository.findAnyByRentedEqual(false);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNumberOfAvailableCars() {
        Long count = carRepository.getNumberOfAvailableCars();
        return count.intValue();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void update(Car car) {
        Car c = em.merge(car);
        carRepository.save(c);
    }

}

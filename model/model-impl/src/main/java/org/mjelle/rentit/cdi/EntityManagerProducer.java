package org.mjelle.rentit.cdi;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.apache.deltaspike.jpa.api.transaction.TransactionScoped;

public class EntityManagerProducer {

    @PersistenceUnit(unitName = "RentIT")
    private EntityManagerFactory emf;

    @Produces
    @TransactionScoped
    public EntityManager create() {
        return emf.createEntityManager();
    }

    public void close(@Disposes EntityManager em) {
        if (em.isOpen()) {
            em.close();
        }
    }

}

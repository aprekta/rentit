package org.mjelle.rentit.steps;

import javax.ejb.EJB;
import org.mjelle.rentit.model.entity.Car;
import org.mjelle.rentit.service.CarService;

public class RentACarSupport {

    @EJB
    private CarService carService;

    public void createCars(int availableCars) {
        for (int i = 0; i < availableCars; i++) {
            Car car = new Car();
            carService.create(car);
        }
    }

    public void rentACar() {
        Car car = carService.findAvailableCar();
        car.setRented(true);
        carService.update(car);
    }

    public int getAvailableNumberOfCars() {
        return carService.getNumberOfAvailableCars();
    }
}

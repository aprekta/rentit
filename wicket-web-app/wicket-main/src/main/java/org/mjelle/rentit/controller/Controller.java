package org.mjelle.rentit.controller;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import org.mjelle.rentit.model.entity.Car;
import org.mjelle.rentit.service.CarService;

@ApplicationScoped
@Default
public class Controller {

    @EJB
    private CarService carService;

    public void createCar() {
        Car car = new Car();
        carService.create(car);
    }

    public void rentCar() {
        Car car = carService.findAvailableCar();
        car.setRented(true);
        carService.update(car);
    }

    public int getNumberOfAvailableCars() {
        return carService.getNumberOfAvailableCars();
    }
}

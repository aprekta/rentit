package org.mjelle.rentit;

import org.apache.wicket.cdi.CdiConfiguration;
import org.apache.wicket.protocol.http.WebApplication;
import org.mjelle.rentit.view.Available;

public class Application extends WebApplication {

    @Override
    protected void init() {
        super.init();

        new CdiConfiguration().configure(this);
    }

    @Override
    public Class<Available> getHomePage() {
        return Available.class;
    }
}

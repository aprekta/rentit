package org.mjelle.rentit.view;

import javax.inject.Inject;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.util.value.ValueMap;
import org.mjelle.rentit.controller.Controller;

public class Rent extends WebPage {

    @Inject
    private Controller controller;

    public Rent() {
        RentCarForm rentCarForm = new RentCarForm("rentCarForm");
        add(rentCarForm);
    }

    private class RentCarForm extends Form<ValueMap> {

        public RentCarForm(String id) {
            super(id, new CompoundPropertyModel<>(new ValueMap()));
        }

        @Override
        public final void onSubmit() {
            controller.rentCar();

            setResponsePage(Available.class);
        }
    }
}

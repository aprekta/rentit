package org.mjelle.rentit.view;

import javax.inject.Inject;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.PropertyModel;
import org.mjelle.rentit.controller.Controller;

public class Available extends WebPage {

    @Inject
    private Controller controller;

    public Available() {
        add(new Label("availableCars", new PropertyModel(this, "controller.numberOfAvailableCars")));
    }
}

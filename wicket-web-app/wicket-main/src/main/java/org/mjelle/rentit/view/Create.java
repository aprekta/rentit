package org.mjelle.rentit.view;

import javax.inject.Inject;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.util.value.ValueMap;
import org.mjelle.rentit.controller.Controller;

public class Create extends WebPage {

    @Inject
    private Controller controller;

    public Create() {
        CreateCarsForm createCarsForm = new CreateCarsForm("createCarsForm");
        add(createCarsForm);
    }

    private class CreateCarsForm extends Form<ValueMap> {

        public CreateCarsForm(String id) {
            super(id, new CompoundPropertyModel<>(new ValueMap()));

            FormComponent<Integer> textField = new TextField<>("numberOfCarsField");
            textField.setType(String.class);

            add(textField);
        }

        @Override
        public final void onSubmit() {
            ValueMap values = getModelObject();

            String addedCars = (String) values.get("numberOfCarsField");

            int numberOfCars = Integer.parseInt(addedCars);
            
            for (int i = 0; i < numberOfCars; i++) {
                controller.createCar();
            }

            setResponsePage(Available.class);
        }
    }
}

package org.mjelle.rentit.controller;

import org.mjelle.rentit.AbstractTest;
import javax.inject.Inject;
import static org.hamcrest.CoreMatchers.is;
import org.jboss.arquillian.junit.Arquillian;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ControllerTest extends AbstractTest {
    
    @Inject
    private Controller controller;

    @Test
    public void shouldRentACar() {
        int initialNumberOfCars = 43;
        
        for(int i = 0; i < initialNumberOfCars; i++) {
            controller.createCar();
        }

        int oneRentedCar = 1;
        int expected = initialNumberOfCars - oneRentedCar;

        controller.rentCar();

        int actual = controller.getNumberOfAvailableCars();

        assertThat(actual, is(expected));
    }
}

package org.mjelle.rentit.view;

import org.mjelle.rentit.controller.*;
import org.mjelle.rentit.AbstractTest;
import javax.inject.Inject;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class CreateTest extends AbstractTest {
    
    @Inject
    private Controller controller;
    
    @Test
    public void pageRenders() {
        wicketTester.startPage(Create.class);
        wicketTester.assertRenderedPage(Create.class);
    }
}

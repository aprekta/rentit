package org.mjelle.rentit;

import java.io.File;
import javax.inject.Inject;
import org.apache.wicket.util.tester.WicketTester;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;

public abstract class AbstractTest {

    @Inject
    private Application application;

    protected WicketTester wicketTester;

    @Deployment
    public static WebArchive createDeployment() {
        File[] files = Maven.resolver().loadPomFromFile("pom.xml")
                .importRuntimeDependencies().resolve().withTransitivity().asFile();

        WebArchive war = ShrinkWrap.create(WebArchive.class, "wicket-main.war");
        war.merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class)
                .importDirectory("src/main/webapp").as(GenericArchive.class),
                "/", Filters.includeAll());

        war.addPackages(false, "org.mjelle.rentit", "org.mjelle.rentit.controller", "org.mjelle.rentit.view");
        war.addAsResource(new File("src/main/resources/"), "");
        war.addAsLibraries(files);
        return war;
    }

    @Before
    public void setup() {
        wicketTester = new WicketTester(application);
    }
}

package org.mjelle.rentit;

import cucumber.api.CucumberOptions;
import cucumber.runtime.arquillian.ArquillianCucumber;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import java.io.File;
import org.apache.deltaspike.cdise.api.CdiContainer;
import org.apache.deltaspike.cdise.api.CdiContainerLoader;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mjelle.rentit.steps.RentStepdefs;

@Features({"org/mjelle/rentit/Rent.feature"})
@Glues({RentStepdefs.class})
@RunWith(ArquillianCucumber.class)
@CucumberOptions(format = {"pretty", "html:target/cucumber"})
public class RunCukesIT {
    
    private CdiContainer cdiContainer;
    
    @Deployment(testable = false)
    public static WebArchive createDeployment() {
        File archive = Maven.resolver().loadPomFromFile("pom.xml").resolve("org.mjelle.rentit:jsf-main:war:?").withoutTransitivity().asSingleFile();
        WebArchive war = ShrinkWrap.create(ZipImporter.class, "jsf-main.war").importFrom(archive)
                .as(WebArchive.class);
        return war;
    }
    
    @Before
    @RunAsClient
    public void setup() {
        cdiContainer = CdiContainerLoader.getCdiContainer();
        cdiContainer.boot();

    }
    
    @After
    @RunAsClient
    public void teardown() {
        cdiContainer.shutdown();
    }
}

package org.mjelle.rentit;

import java.util.logging.Logger;
import org.apache.deltaspike.cdise.api.CdiContainer;
import org.apache.deltaspike.cdise.api.CdiContainerLoader;
import org.apache.deltaspike.core.api.provider.BeanProvider;
import org.mjelle.rentit.view.MainFrame;

public class Main {
    
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        LOGGER.info("Starting client");

        CdiContainer cdiContainer = CdiContainerLoader.getCdiContainer();
        cdiContainer.boot();

        // You can use CDI here
        MainFrame mainFrame = BeanProvider.getContextualReference(MainFrame.class, false);
        mainFrame.setVisible(true);

        // Shutdown CDI container
        cdiContainer.shutdown();
    }
}

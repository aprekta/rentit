package org.mjelle.rentit;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import org.mjelle.rentit.service.CarService;

public class CarServiceProvider {
    private final Context ctx;

    public CarServiceProvider() throws NamingException {
        ctx = new InitialContext();
    }

    @Produces
    @Dependent
    public CarService lookupCarService() throws NamingException {
        Object ref = ctx.lookup("CarServiceImplRemote");
        return (CarService) PortableRemoteObject.narrow(ref, CarService.class);
    }
}

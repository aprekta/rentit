package org.mjelle.rentit.view;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.mjelle.rentit.controller.MainController;

public class MainFrame extends JFrame {

    @Inject
    private MainController controller;
    
    private JTextField numberOfCars;

    public MainFrame() {
    }
    
    @PostConstruct
    public void init() {
        controller.setMainFrame(this);
        addMenu();
        createMainFrame();
        controller.showAvailableCars();
    }

    public void showAvailableCars(int availableCars) {
        clearContentPanel();

        JPanel contentPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        addAvailableCarsLeadingText(contentPanel);
        addAvailableCarsLabel(availableCars, contentPanel);

        add(contentPanel);
    }

    private void addAvailableCarsLeadingText(JPanel contentPanel) {
        JLabel availableCarsLabel = new JLabel();
        availableCarsLabel.setText("Available compact cars: ");
        contentPanel.add(availableCarsLabel);
    }

    private void addAvailableCarsLabel(int availableCars, JPanel contentPanel) {
        JLabel availableCarsValueLabel = new JLabel();
        availableCarsValueLabel.setName("availableCarsValueLabel");
        availableCarsValueLabel.setText("" + availableCars);
        contentPanel.add(availableCarsValueLabel);
    }

    public void showCreateCars() {
        clearContentPanel();

        JPanel contentPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        addNumberOfCarsLabel(contentPanel);
        addNumberOfCarsField(contentPanel);
        addCreateButton(contentPanel);

        add(contentPanel);
    }

    private void addNumberOfCarsLabel(JPanel contentPanel) {
        JLabel availableCarsLabel = new JLabel();
        availableCarsLabel.setText("Number of cars: ");
        contentPanel.add(availableCarsLabel);
    }

    private void addNumberOfCarsField(JPanel contentPanel) {
        numberOfCars = new JTextField("            ");
        numberOfCars.setName("numberOfCars");
        contentPanel.add(numberOfCars);
    }

    private void addCreateButton(JPanel contentPanel) {
        JButton createButton = new JButton("Create cars");
        createButton.setName("createButton");
        createButton.setActionCommand("createCars");
        createButton.addActionListener(controller);
        contentPanel.add(createButton);
    }

    public Component add(JPanel component) {
        super.add(component);
        component.revalidate();
        return component;
    }

    public JTextField getNumberOfCarsTextField() {
        return numberOfCars;
    }

    private void addMenu() {
        JMenuBar menu = new JMenuBar();
        setMenuLayout(menu);
        addMenuItems(menu);
        setJMenuBar(menu);
    }

    private void createMainFrame() {
        setSize(400, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void setMenuLayout(JMenuBar menu) {
        LayoutManager layoutManager = new FlowLayout(FlowLayout.LEFT);
        menu.setLayout(layoutManager);
    }

    private void addMenuItems(JMenuBar menu) {
        menu.add(getFileMenu());
        menu.add(getRentMenu());
        menu.add(getToolsMenu());
    }

    private JMenu getFileMenu() {
        JMenu file = new JMenu("File");
        JMenuItem exit = new JMenuItem("Exit");
        exit.setActionCommand("exit");
        exit.addActionListener(controller);
        file.add(exit);

        return file;
    }

    private JMenuItem getRentMenu() {
        JMenuItem rent = new JMenuItem("Rent");
        rent.setName("rentMenuItem");
        rent.setActionCommand("rentCar");
        rent.addActionListener(controller);

        return rent;
    }

    private JMenu getToolsMenu() {
        JMenu tools = new JMenu("Tools");
        JMenuItem addCars = new JMenuItem("Add cars");
        addCars.setName("showAddCarsForm");
        addCars.setActionCommand("showAddCarsForm");
        addCars.addActionListener(controller);
        tools.add(addCars);

        return tools;
    }

    private void clearContentPanel() {
        getContentPane().removeAll();
    }
}

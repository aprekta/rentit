package org.mjelle.rentit.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.JTextField;
import org.mjelle.rentit.model.entity.Car;
import org.mjelle.rentit.service.CarService;
import org.mjelle.rentit.view.MainFrame;

@Singleton
public class MainController implements ActionListener {
    
    private static final Logger LOGGER = Logger.getLogger(MainController.class.getName());

    @Inject
    private CarService carService;
    
    private MainFrame mainFrame;
    
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String actionCommand = actionEvent.getActionCommand();

        if (actionCommand.equalsIgnoreCase("showAddCarsForm")) {
            showAddCarsForm();
        } else if (actionCommand.equalsIgnoreCase("createCars")) {
            createCars();
        } else if (actionCommand.equalsIgnoreCase("rentCar")) {
            rentCar();
        } else if (actionCommand.equalsIgnoreCase("exit")) {
            exit();
        } else {
            LOGGER.log(Level.WARNING, "Unknown action command: {0}", actionCommand);
        }
    }

    private void showAddCarsForm() {
        mainFrame.showCreateCars();
    }

    private void createCars() {
        JTextField textField = mainFrame.getNumberOfCarsTextField();
        String carsToCreateString = textField.getText().trim();
        int carsToCreate = Integer.parseInt(carsToCreateString);
        for (int i = 0; i < carsToCreate; i++) {
            Car car = new Car();
            carService.create(car);
        }
        int numberOfAvailableCars = carService.getNumberOfAvailableCars();
        mainFrame.showAvailableCars(numberOfAvailableCars);
    }

    public void showAvailableCars() {
        int numberOfAvailableCars;
        try {
            numberOfAvailableCars = carService.getNumberOfAvailableCars();
        } catch (RuntimeException e) {
            numberOfAvailableCars = 0;
        }
        mainFrame.showAvailableCars(numberOfAvailableCars);
    }

    private void rentCar() {
        int numberOfAvailableCars;
        try {
            Car car = carService.findAvailableCar();
            car.setRented(true);
            carService.update(car);
            numberOfAvailableCars = carService.getNumberOfAvailableCars();
        } catch (RuntimeException e) {
            numberOfAvailableCars = 0;
        }
        mainFrame.showAvailableCars(numberOfAvailableCars);
    }

    private void exit() {
        System.exit(0);
    }

    public void setMainFrame(MainFrame view) {
        this.mainFrame = view;
    }
}

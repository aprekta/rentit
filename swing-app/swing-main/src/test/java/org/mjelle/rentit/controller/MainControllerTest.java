package org.mjelle.rentit.controller;

import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mjelle.rentit.model.entity.Car;
import org.mjelle.rentit.service.CarService;
import org.mjelle.rentit.view.MainFrame;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MainControllerTest {

    @Mock
    private CarService carService;
    @Mock
    private MainFrame mainFrame;

    @InjectMocks
    private MainController instance;

    @Test
    public void testActionPerformedShowAddCarsForm() {
        ActionEvent actionEvent = new ActionEvent(this, 0, "showAddCarsForm");
        instance.actionPerformed(actionEvent);

        verify(mainFrame).showCreateCars();
    }

    @Test
    public void testActionPerformedCreateCars() {
        int carsReturned = 24;
        
        JTextField mockField = mock(JTextField.class);
        when(mainFrame.getNumberOfCarsTextField()).thenReturn(mockField);
        when(mockField.getText()).thenReturn(Integer.toString(carsReturned));
        when(carService.getNumberOfAvailableCars()).thenReturn(carsReturned);
        
        ActionEvent actionEvent = new ActionEvent(this, 1, "createCars");
        instance.actionPerformed(actionEvent);

        verify(mainFrame).getNumberOfCarsTextField();
        verify(carService, times(carsReturned)).create(any(Car.class));
        verify(carService).getNumberOfAvailableCars();
        verify(mainFrame).showAvailableCars(carsReturned);
    }

    @Test
    public void testActionPerformedRentCar() {
        int carsReturned = 99;
        
        Car mockCar = mock(Car.class);
        when(carService.findAvailableCar()).thenReturn(mockCar);
        when(carService.getNumberOfAvailableCars()).thenReturn(carsReturned);
        
        ActionEvent actionEvent = new ActionEvent(this, 2, "rentCar");
        instance.actionPerformed(actionEvent);

        verify(carService).findAvailableCar();
        verify(mockCar).setRented(true);
        verify(carService).update(mockCar);
        verify(carService).getNumberOfAvailableCars();
        verify(mainFrame).showAvailableCars(carsReturned);

    }

    @Test
    public void testShowAvailableCars() {
        int carsReturned = 27;
        when(carService.getNumberOfAvailableCars()).thenReturn(carsReturned);

        instance.showAvailableCars();

        verify(carService).getNumberOfAvailableCars();
        verify(mainFrame).showAvailableCars(carsReturned);
    }
}
